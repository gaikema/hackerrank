# Hacker Rank
Working on problems on HackerRank. My profile is  [here](https://www.hackerrank.com/mgaikema1).

## Instructions

Every file is a self-contained script, in either Python or C++.
To run a file, 
go to the command prompt in the directory that contains that file and enter
```
mkdir _build
cd _build
cmake ../
```
then run the file in Visual Studio or Make or whatever.
Some of the executables can be found [here](https://www.dropbox.com/sh/8bdvorp2tiv1pr9/AACui7wdFNgw_0rqikeJHty-a?dl=0).

## Caveat
Copying my code robs you of the opportunity to learn. 
Also, some of these don't work.