/*
	https://www.hackerrank.com/contests/rookierank/challenges/antiprime-numbers
*/

#include <math.h>
#include <vector>
#include <iostream>
#include <algorithm>
#include <sstream>

using namespace std;

int main() 
{
	int q;
	cin >> q;

	vector<int> a;
	for (int i = 0; i < q; i++)
	{
		int b;
		cin >> b;
		a.push_back(b);
	}

	// http://oeis.org/A002182
	// http://wwwhomes.uni-bielefeld.de/achim/highly.txt
	vector<int> antiprimeNumbers = {1, 2, 4, 6, 12, 24, 36, 48, 60, 120, 180, 240, 360, 720, 840, 1260, 1680, 2520, 5040, 7560, 10080, 15120, 20160, 25200, 27720, 45360, 50400, 55440, 83160, 110880, 166320, 221760, 277200, 332640, 498960, 554400, 665280, 720720, 1081080, 1441440, 2162160, 2882880, 3603600, 4324320, 6486480, 7207200, 8648640, 10810800, 14414400, 17297280, 21621600, 32432400, 36756720, 43243200, 61261200, 73513440};

	stringstream ss;

	for (int i = 0; i < a.size(); i++)
	{
		//cout << "ai=" << a[i] << endl;
		for (int j = 0;  j < antiprimeNumbers.size(); j++)
		{
			//cout << "antiprimeNumbers[j]=" << antiprimeNumbers[j] << endl;
			if (a[i] <= antiprimeNumbers[j])
			{
				ss << antiprimeNumbers[j] << endl;
				break;
			}
		}
	}
	cout << ss.str();

	return 0;
}
