/*
	https://www.hackerrank.com/contests/rookierank/challenges/birthday-cake-candles
*/

#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
	int n;
	cin >> n;
	vector<int> height(n);
	for(int height_i = 0;height_i < n;height_i++)
		cin >> height[height_i];
	
	int max = *max_element(height.begin(), height.end());
	int times = count(height.begin(), height.end(), max);

	cout << times;

	return 0;
}