/*
	https://www.hackerrank.com/contests/rookierank/challenges/extremely-dangerous-virus
*/

import java.io.*;
import java.util.*;
import java.math.*;

// https://www.hackerrank.com/contests/rookierank/challenges/extremely-dangerous-virus/forum/comments/160123
public class Solution 
{

    public static void main(String[] args) 
	{
		Scanner scanner = new Scanner(System.in);
		int a = scanner.nextInt();
		int b = scanner.nextInt();
		long t = scanner.nextLong();

		long mod = (long)Math.pow(10,9)+7;

		// http://www.geeksforgeeks.org/biginteger-class-in-java/
		BigInteger b1, b2, b3, b4;
		b1 = BigInteger.valueOf((long)(a+b)/2);
		b2 = BigInteger.valueOf(t);
		b3 = BigInteger.valueOf(mod);
		b4 = b1.modPow(b2,b3);

		System.out.println(b4);
    }
}