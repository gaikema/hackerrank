/*
    https://www.hackerrank.com/contests/w22/challenges/sequential-prefix-function
*/

#include <iostream>
#include <vector>

using namespace std;

/*
* Knuth-Morris-Pratt algorithm.
* http://cs.indstate.edu/~kmandumula/presentation.pdf
* http://stackoverflow.com/questions/13792118/kmp-prefix-table
*/
vector<int> kmp(vector<int> p)
{
    int m = p.size();
    // Prefix function.
    vector<int> a(m);
    int k = 0;

    for (int q = 1; q < m; q++)
    {
        while (k > 0 && p[k] != p[q])
        {
            k = a[k-1];
        }
        if (p[k] == p[q])
        {
            k++;
        }
        a[q] = k;
    }

    return a;
}

int main()
{
    // Number of queries.
    int n;
    cin >> n;

    vector<int> seq;

    for (int i = 0; i < n; i++)
    {
        char op;
        cin >> op;
        
        if (op == '+')
        {
            int x;
            cin >> x;
            seq.push_back(x);

            vector<int> pre = kmp(seq);
            // Print out the last element.
            cout << pre[pre.size() - 1] << endl;
        }
        else if (op == '-')
        {
            seq.pop_back();

            vector<int> pre = kmp(seq);
            if (pre.size() != 0)
            {
                // Print out the last element.
                cout << pre[pre.size() - 1] << endl;
            }
            else
                cout << 0 << endl;           
        }
    }

    return 0;
}