/*
    https://www.hackerrank.com/contests/w22/challenges/polygon-making
*/

#include <vector>
#include <iostream>
#include <algorithm>
#include <numeric>

using namespace std;

int main()
{
    // Number of sides.
    int n;
    cin >> n;

    // Lengths of the sides of the polygon.
    vector<int> a(n);
    for (int i = 0; i < n; i++)
    {
        cin >> a[i];
    }

    //Sort the sides to make this faster.
    sort(a.begin(), a.end());

    // The number of cuts to make.
    int cuts = 0;

    /*
        The longest side of the polygon must be less
        than the sum of all of the other sides:
        http://math.stackexchange.com/questions/413764/polygon-inequality

        We must keep making cuts on the largest side 
        until this inequality is satisfied.

        The algorithm goes like this:
        while (sum(other_sides) <= longest_side)
        {
            cuts++;
            cut(longest_side); // Rotate the array once.
        }
    */

    int longest_side = a[a.size()-1];
    int sum_other_sides = accumulate(a.begin(), a.end()-1, 0);
    while (longest_side >= sum_other_sides)
    {
        cuts++;
        rotate(a.rbegin(), a.rend()+1, a.rend());
        longest_side = a[a.size()-1];
        sum_other_sides = accumulate(a.begin(), a.end()-1, 0);
    }

    cout << cuts << endl;

    return 0;
}