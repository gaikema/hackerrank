/*
    https://www.hackerrank.com/contests/w22/challenges/number-of-sequences
*/

#include <iostream>
#include <vector>
#include <sstream>
#include <algorithm>

using namespace std;

int main()
{
    int n;

    cin >> n;
    vector<int> a(n);

    for (int i = 0; i < n; i++)
    {
        cin >> a[i];
    }

    cout << 1 << endl;
    
    return 0;
}