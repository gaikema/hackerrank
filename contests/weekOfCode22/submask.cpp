/*
    https://www.hackerrank.com/contests/w22/challenges/submask-queries-
*/

#include <iostream>
#include <vector>
#include <sstream>
#include <string>
#include <unordered_map>
#include <algorithm>
#include <math.h>

using namespace std;

// https://codeaccepted.wordpress.com/2014/01/14/algorithm-4-bit-masking/
vector<string> get_subsets(string str)
{
    int j;
    long long temp, i, current_sum, last;
    int set_size = count(str.begin(), str.end(), '1');
    last = pow(2, set_size);

    vector<string> subs;

    for (i = 0; i < last; i++)
    {
       temp = i;
       
        string subset;
        /*
        for (int j = 0; j < str.size(); j++)
            subset.push_back('0');
        */

        j = 0;
        while (j < str.size())
        {
            subset.push_back('0');
            if (temp % 2 == 1)
            {
                subset[j] = '1';
            }
            temp>>=1;
            j++;
        }
        subs.push_back(subset);
    }

    return subs;
}

int main()
{
    // Size of the set.
    int n;
    // Number of queries to perform.
    int m;
    cin >> n >> m;
    cin.ignore();
    
    stringstream output;

    unordered_map<string, int> subsets;

    // Do the queries.
    for (int i = 0; i < m; i++)
    {
        string str;
        getline(cin, str);

        stringstream ss(str);
        int c_num;
        ss >> c_num;

        if (c_num == 1)
        {
            int x;
            string sub;
            ss >> x >> sub;

            vector<string> subs = get_subsets(sub);
            for (auto k : subs)
            {
                subsets[k] = x;
            }
        } // !if
        else if (c_num == 2)
        {
            int x;
            string sub;
            ss >> x >> sub;

            vector<string> subs = get_subsets(sub);
            for (auto k : subs)
            {
                subsets[k] = subsets[k]^x;
            }
        } // !if
        else // if c_num == 3
        {
            string sub;
            ss >> sub;
            output << subsets[sub] << endl;
        }
    }

    cout << output.str();
    
    return 0;
}