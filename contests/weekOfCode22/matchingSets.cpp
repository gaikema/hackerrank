/*
	https://www.hackerrank.com/contests/w22/challenges/box-moving
*/

#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <sstream>
#include <stdlib.h>

using namespace std;

// Get the absolute values of the differences between each element.
vector<int> vec_difference(vector<int> v1, vector<int> v2)
{
	vector<int> diff;
	for (int i = 0; i < v1.size(); i++)
	{
		diff.push_back(abs(v1[i] - v2[i]));
	}

	return diff;
}

void print_vec(vector<int> vec)
{
	for (auto k : vec)
	{
		cout << k << " ";
	}
	cout << endl;
}

int main()
{
	stringstream ss;
	int n;
	cin >> n;

	vector<int> x(n);
	vector<int> y(n);

	for (int i = 0; i < n; i++)
	{
		cin >> x[i];
	}
	for (int i = 0; i < n; i++)
	{
		cin >> y[i];
	}

	int sum_x = accumulate(x.begin(), x.end(), 0);
	int sum_y = accumulate(y.begin(), y.end(), 0);

	/*
		If the sums of them are different,
		no amount of operations will make them equal.
	*/
	if (sum_x != sum_y)
	{
		// Print -1, since they will never be equal.
		ss << -1 << endl;
	}
	else
	{
		sort(x.begin(), x.end());
		sort(y.begin(), y.end());

		/*
			If x[i] < y[i],
			find j such that x[j] > y[j] and
			x[i]++, x[j]--.
			Do the reverse if x[i] > y[i].
		*/

		int num_op = 0;
		for (int i = 0; i < n; i++)
		{
			while (x[i] != y[i])
			{
				if (x[i] < y[i])
				{
					for (int j = i; j < n; j++)
					{
						if (x[j] > y[j])
						{
							x[i]++;
							x[j]--;
							num_op++;
							break;
						} // !if
					} // !for
				} // !if

				else if (x[i] > y[i])
				{
					for (int j = i; j < n; j++)
					{
						if (x[j] < y[j])
						{
							x[i]--;
							x[j]++;
							num_op++;
							break;
						} // !if
					} // !for
				} // !if
			} // !while
		}

		ss << num_op << endl;
	}

	cout << ss.str();

	return 0;
}