/*
    https://www.hackerrank.com/contests/w22/challenges/subtree-expectation
*/

#include <iostream>
#include <vector>
#include <algorithm>
#include <unordered_map>
#include <numeric>
#include <sstream>

using namespace std;

int main()
{
    // Number of queries.
    int q;
    cin >> q;

    stringstream ss;

    for (int i = 0; i < q; i++)
    {
        // Number of vertices in the tree.
        int n;
        cin >> n;

        int s_wv = 0;
        // Maps a vertex to its weight.
        unordered_map<int, int> weights;

        // Get the weights of each vertex.
        for (int j = 0; j < n; j++)
        {
            cin >> weights[j];
            s_wv += weights[j];
        }

        unordered_map<int, int> a;
        long total = 0;

        for (int j = 0; j < s_wv + 1; j++)
        {
            int a_x;
            cin >> a_x;
            total += a_x;
        }

        // The matrix.
        vector< vector<int> > tree(n, vector<int>(n));

        // Fill the tree.
        for (int j = 0; j < n - 1; j++)
        {
            int u,v;
            cin >> u >> v;

            tree[u][v] = weights[u] + weights[v];
            //tree[v][u] = tree[u][v];
        }
    }
    
    return 0;
}