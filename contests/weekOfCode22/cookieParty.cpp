/*
    https://www.hackerrank.com/contests/w22/challenges/cookie-party
*/

#include <iostream>

using namespace std;

int main()
{   
    // The number of guests.
    int n;
    // The number of cookies.
    int m;

    cin >> n >> m;

    // The number of additional cookies.
    int k = 0;

    // Add cookies until we can divide evenly.
    while ((m+k) % n != 0 || (m+k) < n)
    {
        k++;
    }

    cout << k << endl;

    return 0;
}