/*
	https://www.hackerrank.com/tests/29533gf3cf5/questions/ear36dioqs9
*/

#include <iostream>
#include <sstream>
#include <time.h>
#include <string>
#include <ctime>
#include <iomanip>

struct date
{
	int year;
	int month;
	int day;
};

int main()
{
	struct std::tm when = {};
	std::string str = "2015-08-15";
	std::stringstream ss(str);
	ss >> std::get_time(&when, "%y-%d-%m");
}