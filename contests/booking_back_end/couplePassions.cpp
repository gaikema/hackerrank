/*
    https://www.hackerrank.com/contests/booking-passions-hacked-backend/challenges/a-couple-and-their-passions
*/

#include <iostream>
#include <unordered_map>
#include <vector>
#include <string>
#include <math.h>
#include <utility>
#include <algorithm>
#include <limits>

using namespace std;

// JSON-type class for locations.
struct Destination
{
    string name;
    pair<double, double> location;
    vector<string> activities;
};

/*
Destination& operation=(const Destination& dest)
{
    this.name = dest.name;
    this.location = dest.location;
    this.activities = dest.activities;
    return *this;
}
*/

// Get the size of the intersection of two vectors.
int num_overlap(vector<string> vec1, vector<string> vec2)
{
    vector<string> v;
    vector<string>::iterator it = set_intersection(vec1.begin(), vec1.end(), vec2.begin(), vec2.end(), v.begin());
    v.resize(it-v.begin());

    return v.size();
}

int main()
{
    const double PI = 3.1415926359;
    
    // Number of guests.
    int n;
    cin >> n;

    vector< vector<string> > guests(n);
    // Number of time each passion occurs.
    unordered_map<string, int> passion_counts;
    for (int i = 0; i < n; i++)
    {
        // Number of passions the guest has.
        int k;
        cin >> k;

        for (int j = 0; j < k; j++)
        {
            string passion;
            cin >> passion;
            guests[i].push_back(passion);
            passion_counts[passion]++;
        }
        //sort(guests[i].begin(), guests[i].end());
    }

    // Get all the passions into a vector with unique elements.
    vector<string> all_passions;
    for (unordered_map<string, int>::iterator it = passion_counts.begin(); it != passion_counts.end(); it++)
    {
        all_passions.push_back(it->first);
    }
    sort(all_passions.begin(), all_passions.end());

    int num_locations;
    cin >> num_locations;
    vector<Destination> destinations;
    for (int i = 0; i < num_locations; i++)
    {
        Destination dest;
        string name;
        double x, y;
        cin >> name >> x >> y;
        dest.name = name;
        dest.location = pair<double, double> (x,y);

        // Number of activities for the location.
        int k;
        cin >> k;
        for (int j = 0; j < k; j++)
        {
            string str;
            cin >> str;
            dest.activities.push_back(str);
        }
        sort(dest.activities.begin(), dest.activities.end());
    }

    /*
        Now, we have all of the information.
        We need to chose the locations with the 
        most activities in common with the guests.
    */

    // Sort the destinations based upon how many activities they have in common with the guests.
    sort(destinations.begin(), destinations.end(), [all_passions](Destination d1, Destination d2){
        int d1_overlap = num_overlap(d1.activities, all_passions);
        int d2_overlap = num_overlap(d2.activities, all_passions);
        return (d1_overlap > d2_overlap);
    });

    int max_overlap = num_overlap(all_passions, destinations[0].activities);
    vector<Destination>::iterator bound = partition(destinations.begin(), destinations.end(), [max_overlap, all_passions](Destination d){
        return (max_overlap == num_overlap(all_passions, d.activities));
    });

    vector<Destination> close_destinations;
    for (vector<Destination>::iterator it = destinations.begin(); it != bound; it++)
    {
        close_destinations.push_back(*it);
    }

    for (auto k : close_destinations)
    {
        cout << k.name << " ";
    }

    return 0;
}