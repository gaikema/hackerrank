/*
    https://www.hackerrank.com/contests/booking-passions-hacked-backend/challenges/destinations-together-3
*/

#include <algorithm>
#include <iostream>
#include <math.h>

using namespace std;

int main()
{
    // Number of places John wants to visit.
    int n;
    // Number of places Zizi wants to visit.
    int m;
    // Number of places in common.
    int c;

    cin >> n >> m >> c;

    int john_unique = n - c;
    int zizi_unique = m - c;
    int total_unique = c + john_unique + zizi_unique;
    cout << tgamma(total_unique) << endl;
    
    return 0;
}