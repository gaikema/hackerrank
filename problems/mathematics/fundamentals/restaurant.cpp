/*
	https://www.hackerrank.com/challenges/restaurant
*/

#include <string>
#include <iostream>
#include <sstream>
#include <math.h>
#include <set>
#include <numeric>

using namespace std;

int gcd(int a, int b)
{
  int c = a % b;
  while(c != 0)
  {
    a = b;
    b = c;
    c = a % b;
  }
  return b;
}


// https://www.hackerrank.com/challenges/restaurant/forum/comments/57495
int main()
{
	int t;
	cin >> t;

	stringstream ss;
	for (int i = 0; i < t; i++)
	{
		int l, b;
		cin >> l >> b;
		int g = gcd(b,l);
		ss << (l/g)*(b/g) << endl;
	}

	cout << ss.str();
}