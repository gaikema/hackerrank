#include <string>
#include <iostream>
#include <vector>
#include <stack>
#include <algorithm>
#include <set>
#include <sstream>

using namespace std;

/*
	Although my solution is optimized for time,
	its space complexity is lacking.
	The solution [here](https://www.hackerrank.com/challenges/maximum-element/forum/comments/120135)
	creates a node class which contains the current max element at the time of creation.
*/
int main()
{
	int n;
	cin >> n;
	stack<int> stk;
	multiset<int> sorted;
	stringstream ss;

	for (int i = 0; i < n; i++)
	{
		int command;
		cin >> command;
		if (command == 1)
		{
			int x;
			cin >> x;
			stk.push(x);
			sorted.insert(x);
		}
		else if (command == 2)
		{
			int x = stk.top();
			stk.pop();
			multiset<int>::iterator it = sorted.find(x);
			sorted.erase(x);
		}
		else
		{
			// Should be constant time.
			// http://stackoverflow.com/a/1342077/5415895
			ss << *sorted.rbegin() << endl;
		}
	}

	cout << ss.str();
}