/*
	https://www.hackerrank.com/challenges/balanced-brackets
*/

#include <string>
#include <iostream>
#include <algorithm>
#include <stack>
#include <sstream>

using namespace std;

bool is_balanced(string str)
{
	stack<char> chars;
	for (string::iterator it = str.begin(); it != str.end(); it++)
	{
		if (*it == '(' || *it == '{' || *it == '[')
		{
			chars.push(*it);
		}
		else if (*it == ')')
		{
			if (chars.empty() || chars.top() != '(')
				return false;
			chars.pop();
		}
		else if (*it == '}')
		{
			if (chars.empty() || chars.top() != '{')
				return false;
			chars.pop();
		}
		else if (*it == ']')
		{
			if (chars.empty() || chars.top() != '[')
				return false;
			chars.pop();
		}
	}
	
	return chars.empty();
}

int main()
{
	int n;
	cin >> n;
	stringstream ss;

	for (int i = 0; i < n; i++)
	{
		string str;
		cin >> str;
		bool flag = is_balanced(str);
		if (flag)
			ss << "YES" << endl;
		else
			ss << "NO" << endl;
	}

	cout << ss.str();
}