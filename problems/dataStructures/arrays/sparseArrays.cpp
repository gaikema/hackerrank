/*
	https://www.hackerrank.com/challenges/sparse-arrays

	The challenge is supposed to involve sparse arrays
	but I just use a hash map.
*/

#include <iostream>
#include <unordered_map>
#include <algorithm>
#include <vector>
#include <iterator>
#include <string>
#include <sstream>

using namespace std;

int main()
{
	int n;
	cin >> n;

	unordered_map<string, int> hash;

	for (int i = 0; i < n; i++)
	{
		string s;
		cin >> s;
		hash[s]++;
	}

	stringstream ss;
	int q;
	cin >> q;

	for (int i = 0; i < q; i++)
	{
		string s;
		cin >> s;
		ss << hash[s] << endl;
	}

	cout << ss.str();

	return 0;
}