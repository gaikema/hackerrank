/*
	https://www.hackerrank.com/challenges/array-left-rotation
*/

#include <vector>
#include <iostream>
#include <algorithm>
#include <iterator>

using namespace std;

int main()
{
	int n, d;
	cin >> n >> d;
	vector<int> a;
	for (int i = 0; i < n; i ++)
	{
		int k;
		cin >> k;
		a.push_back(k);
	}

	// Is this cheating: http://www.cplusplus.com/reference/algorithm/rotate/
	rotate(a.begin(), a.begin()+d, a.end());

	for(auto k : a)
		cout << k << " ";	

	return 0;
}