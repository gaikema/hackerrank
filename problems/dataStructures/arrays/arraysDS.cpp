/*
	https://www.hackerrank.com/challenges/arrays-ds
*/

#include <vector>
#include <algorithm>
#include <iostream>

using namespace std;

int main()
{
	int n;
	cin >> n;
	vector<int> arr(n);
	for(int arr_i = 0;arr_i < n;arr_i++)
		cin >> arr[arr_i];
	
		// http://www.cplusplus.com/reference/algorithm/reverse/
		reverse(arr.begin(), arr.end());
		for (auto it = arr.begin(); it != arr.end(); it++)
		{
			cout << *it << " ";
		}

	return 0;
}