/*
	https://www.hackerrank.com/challenges/queue-using-two-stacks
*/

#include <string>
#include <iostream>
#include <algorithm>
#include <stack>
#include <sstream>

using namespace std;

/*
	https://www.youtube.com/watch?v=7ArHz8jPglw
*/
class my_queue
{
	stack<int> newest_on_top;
	stack<int> oldest_on_top;

public:
	void enqueue(int n);
	void dequeue();
	int front();
};

void my_queue::enqueue(int n)
{
	newest_on_top.push(n);
}

void my_queue::dequeue()
{
	if (oldest_on_top.empty())
	{
		while (!newest_on_top.empty())
		{
			int k = newest_on_top.top();
			newest_on_top.pop();
			oldest_on_top.push(k);
		}
	}
	oldest_on_top.pop();	
}

int my_queue::front()
{
	// https://youtu.be/7ArHz8jPglw?t=276
	if (oldest_on_top.empty())
	{
		while (!newest_on_top.empty())
		{
			int k = newest_on_top.top();
			newest_on_top.pop();
			oldest_on_top.push(k);
		}
	}
	return oldest_on_top.top();
}

int main()
{
	int q;
	cin >> q;
	stringstream ss;
	my_queue mq;

	for (int i = 0; i < q; i++)
	{
		int command;
		cin >> command;

		if (command == 1)
		{
			int x;
			cin >> x;
			mq.enqueue(x);
		}
		else if (command == 2)
		{
			mq.dequeue();
		}
		else if (command == 3)
		{
			ss << mq.front() << endl;
		}
	}

	cout << ss.str();
}