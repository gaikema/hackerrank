binom :: (Integral a) => a -> a -> a
binom n k = product [k+1..n] `div` product [1..n-k]

triangle :: Int -> [[Int]]
triangle 1 = [[1]]
triangle 2 = [[1],[1,1]]
triangle n = triangle (n-1) ++ [map (\x -> binom n x) [0..n]]

main = do
    q <- getLine
    rows <- triangle (read q :: Int)
    result <- unlines rows
    putStr show result
