/*
	https://www.hackerrank.com/challenges/lonely-integer
*/

#include <iostream>
#include <string>
#include <algorithm>
#include <sstream>
#include <iterator>
#include <map>

using namespace std;

/*
	a^a=0 and a^0=a
	https://www.hackerrank.com/challenges/lonely-integer/forum/comments/26955
*/
int main()
{
	int n;
	cin >> n;

	int total = 0;
	for (int i = 0; i < n; i++)
	{
		int q;
		cin >> q;
		total = total^q;
	}
	cout << total << endl;
}