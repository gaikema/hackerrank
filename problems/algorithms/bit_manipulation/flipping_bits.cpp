/*
	https://www.hackerrank.com/challenges/flipping-bits
*/

#include <bitset>
#include <string>
#include <iostream>
#include <sstream>

using namespace std;

int main()
{
	int t;
	cin >> t;

	stringstream ss;
	for (int i = 0; i < t; i++)
	{
		long l;
		cin >> l;
		bitset<32> bits(l);
		bits.flip();
		ss << bits.to_ulong() << endl;
	}

	cout << ss.str();
}