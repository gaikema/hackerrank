/*
	https://www.hackerrank.com/challenges/and-product
*/

#include <bitset>
#include <string>
#include <math.h>
#include <iostream>
#include <sstream>

using namespace std;

// https://www.hackerrank.com/challenges/and-product/forum/comments/109014s
int main()
{
	int n;
	cin >> n;

	stringstream ss;
	for (int i = 0; i < n; i++)
	{
		int a, b;
		cin >> a >> b;

		int prod = a;
		for (int j = 1; a+j < b; j=2*j)
		{
			prod &= a+j;
		}
		ss << (prod&b) << endl;
	}

	cout << ss.str();
}