/*
	https://www.hackerrank.com/challenges/maxsubarray
*/

#include <vector>
#include <iostream>
#include <algorithm>
#include <iterator>
#include <sstream>

using namespace std;

/* 
	Kadane's Algorithm:
	https://en.wikipedia.org/wiki/Maximum_subarray_problem
	http://www.geeksforgeeks.org/largest-sum-contiguous-subarray/

	The logic of the algorithm is this:
	look for all contiguous positive segments of the array
	(which is what max_ending_here is for),
	and also keep track of the maximum sum contiguous segment
	among all positive segments (which is what max_so_far is for).
	Every time we get a positive sum, 
	compare it with max_so_far and update max_so_far if the sum is greater.

	In my own words,
	this means start at the begining and keep adding elements to max_ending_here,
	resetting max_ending_here when it becomes negative.
	max_so_far represents the maximum of all the sums,
	and is updated after each iteration.
*/
int max_subarray(vector<int> vec)
{
	int max_ending_here = vec[0];
	int max_so_far = vec[0];
	for (int i = 1; i < vec.size(); i++)
	{
		max_ending_here = max(vec[i], max_ending_here + vec[i]);
		max_so_far = max(max_so_far, max_ending_here);
	}

	return max_so_far;
}

int max_sum(vector<int> vec)
{
	sort(vec.begin(), vec.end());
	vector<int>::iterator lower = lower_bound(vec.begin(), vec.end(), 0);
	if (lower == vec.end())
		return *(lower-1);

	int sum = 0;
	for (vector<int>::iterator iter = lower; iter != vec.end(); iter++)
	{
		sum += *iter;
	}

	return sum;
}

int main()
{
	int t;
	cin >> t;

	stringstream ss;

	for (int i = 0; i < t; i++)
	{
		int n;
		cin >>n;

		vector<int> a;
		for (int j = 0; j < n; j++)
		{
			int k;
			cin >> k;
			a.push_back(k);
		}

		ss << max_subarray(a) << " " << max_sum(a) << endl;
	}

	cout << ss.str();

	return 0;
}