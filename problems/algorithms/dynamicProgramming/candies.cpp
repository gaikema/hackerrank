/*
	https://www.hackerrank.com/challenges/candies
*/

#include <vector>
#include <iostream>
#include <string>
#include <algorithm>
#include <numeric>

using namespace std;

// http://stackandqueue.com/?p=108
int main()
{
	int n;
	cin >> n;

	vector<int> children(n);
	for (int i = 0; i < n; i++)
		cin >> children[i];

	// Each kid gets at least one candy.
	vector<int> candies(n, 1);
	
	// Increase count for those on the right of a local minima.
	for (int i = 1; i < n; i++)
	{
		if (children[i] > children[i-1])
			candies[i] = candies[i-1]+1;
	}

	// Increase count for candy on the left of local minimas.
	for (int i = n-2; i >= 0; i--)
	{
		if (children[i] > children[i+1])
			candies[i] = max(candies[i], candies[i+1]+1);
	}

	cout << accumulate(candies.begin(), candies.end(), 0) << endl;
}