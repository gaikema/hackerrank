/*
	https://www.hackerrank.com/challenges/primsmstsub
*/

#include <vector>
#include <iostream>
#include <algorithm>
#include <iterator>
#include <set>
#include <utility>
#include <limits>
#include <map>

using namespace std;

/**
* Uses Prim's algorithm to find the weight of the minimum spanning tree
* of a weighted, undirected graph.
* https://www.youtube.com/watch?v=cplfcGZmX7I
*
* @param vector<vector<int>> matrix The graph's adjacency matrix.
*/
int prim_weight(vector<vector<int>> matrix)
{
	vector<bool> unvisited (matrix.size(), true);
	set<int> visited;

	// Add first node to visited set.
	unvisited[0] = false;
	visited.insert(0);
	int total_weight = 0;

	while (visited.size() < matrix.size())
	{
		/*
			Find the nearest unvisited node and add it.
			This involves finding the unvisited node nearest to each visited node,
			and finding the minimum of that.
		*/
		// Distance from it to node, and the node.
		map<int, int> distances_and_nodes;
		for (set<int>::iterator it = visited.begin(); it != visited.end(); it++)
		{
			int min_length = numeric_limits<int>::max();
			// Node associated with min_length.
			int min_node;
			// Find the nearest node to it.
			for (int j = 0; j < matrix[*it].size(); j++)
			{
				// If j has not been visited and is less than min_length, change min_length.
				if (unvisited[j] == true && matrix[*it][j] < min_length)
				{
					min_length = matrix[*it][j];
					min_node = j;
				}
				distances_and_nodes.insert(pair<int,int>(min_length, min_node));
			}
		}
		int dist = distances_and_nodes.begin()->first;
		int node = distances_and_nodes.begin()->second;
		visited.insert(node);
		unvisited[node] = false;
		total_weight += dist;
	}

	return total_weight;
}

int main()
{
	// Number of nodes.
	int n;
	// Number of edges.
	int m;
	cin >> n >> m;

	// http://stackoverflow.com/a/17663236/5415895
	// http://en.cppreference.com/w/cpp/types/numeric_limits/infinity
	vector<vector<int>> adjacency_matrix(n, vector<int>(n, numeric_limits<int>::max()));
	for(int k = 0; k < m; k++)
	{
		int x,y,r;
		cin >> x >> y >> r;
		adjacency_matrix[x-1][y-1] = r;
		// Make it symetric I guess.
		adjacency_matrix[y-1][x-1] = r;
	}

	// Starting node; I don't care about this.
	int s;
	cin >> s;

	cout << prim_weight(adjacency_matrix);

	return 0;
}