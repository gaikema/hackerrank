CMAKE_MINIMUM_REQUIRED(VERSION 2.6.0)
project(greedy)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -std=c++11")

add_executable(markToys markToys.cpp)

add_executable(luck_balance luck_balance.cpp)

add_executable(priyanka_toys priyanka_toys.cpp)

add_executable(largest_permutation largest_permutation.cpp)

add_executable(florist florist.cpp)

add_executable(max_min max_min.cpp)

add_executable(permuting_arrays permuting_arrays.cpp)

add_executable(jim_orders jim_orders.cpp)