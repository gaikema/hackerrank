/*
	https://www.hackerrank.com/challenges/largest-permutation
*/

#include <vector>
#include <iostream>
#include <string>
#include <algorithm>
#include <iterator>
#include <map>

using namespace std;

// https://en.wikipedia.org/wiki/Permutation#Generation_in_lexicographic_order
int main()
{
	int n, k;
	cin >> n >> k;

	vector<int> vec(n);
	// Maps the element to their position in vec.
	//map<int, int> anti_vec;
	for (int i = 0; i < n; i++)
	{
		cin >> vec[i];
		//anti_vec[vec[i]] = i;
	}

	int m = n;
	int count = 0;
	for (int i = 0; i < vec.size(); i++)
	{
		if (count >= k)
			break;
		// This is correct but too slow. See:
		// https://www.hackerrank.com/challenges/largest-permutation/forum/comments/108177
		vector<int>::iterator it = max_element(vec.begin()+i, vec.end());
		int dist = distance(vec.begin(), it);
		if (vec[i] < *it)
		{
			swap(vec[i], vec[dist]);
			count++;
		}
	}

	for (vector<int>::iterator it = vec.begin(); it != vec.end(); it++)
	{
		cout << *it << " ";
	}
}