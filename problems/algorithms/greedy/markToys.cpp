/*
	https://www.hackerrank.com/challenges/mark-and-toys
*/

#include <vector>
#include <iostream>
#include <set>

using namespace std;

int main()
{
	// Number of toys.
	int n; 
	// Number of dollars.
	int k;
	cin >> n >> k;

	set<int> toys;
	for (int i = 0; i < n; i++)
	{
		int temp;
		cin >> temp;
		toys.insert(temp);
	}

	typedef set<int>::iterator siterator;
	siterator it = toys.begin();
	int money_left = k;
	int toys_bought = 0;

	while (money_left - *it > 0 && it != toys.end())
	{
		money_left -= *it;
		toys_bought++;
		it++;
	}

	cout << toys_bought << endl;
}