#include <vector>
#include <iostream>
#include <string>
#include <algorithm>
#include <iterator>
#include <sstream>
#include <map>

using namespace std;

int main()
{
	int n;
	cin >> n;
	multimap<int, int> orders;
	for (int i = 0; i < n; i++)
	{
		int t, d;
		cin >> t >> d;
		orders.insert(pair<int, int>(t+d, i+1));
	}
	for (multimap<int, int>::iterator it = orders.begin(); it != orders.end(); it++)
	{
		cout << it->second << " ";
	}
}