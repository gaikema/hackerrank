/*
	https://www.hackerrank.com/challenges/angry-children

	https://gist.github.com/TexAgg/65d27aaa391a1fbef77ce7fe92e052ab
*/

#include <vector>
#include <iostream>
#include <string>
#include <algorithm>
#include <iterator>
#include <set>
#include <numeric>
#include <limits>

using namespace std;

int main()
{
	int n;
	int k;
	cin >> n >> k;

	vector<int> vec(n);
	for (int i = 0; i < n; i++)
	{
		cin >> vec[i];
	}

	/*
		We want to find the k elements with the smallest range.
	*/
	sort(vec.begin(), vec.end());
	int curr = numeric_limits<int>::max();
	for (vector<int>::iterator it = vec.begin(); it != vec.end()-k+1; it++)
	{
		int unfairness = *(it+k-1) - *it;
		curr = min(curr, unfairness);
	}

	cout << curr << endl;
}