#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <numeric>

using namespace std;

int main()
{
	// Number of preliminary contests.
	int n;
	// Maximum number of important contests Lena can lose.
	int k;
	cin >> n >> k;

	vector<int> important;
	vector<int> unimportant;
	for (int i = 0; i < n; i++)
	{
		int points;
		bool importance;
		cin >> points >> importance;
		if (importance)
			important.push_back(points);
		else
			unimportant.push_back(points);
	}

	int luck = 0;
	sort(important.begin(), important.end());
	luck += accumulate(unimportant.begin(), unimportant.end(), 0);
	if (n != k)
	{
		luck += accumulate(important.end()-k, important.end(), 0);
		luck -= accumulate(important.begin(), important.end()-k, 0);
	}
	else 
	{
		luck += accumulate(important.begin(), important.end(), 0);
	}
	cout << luck << endl;
}