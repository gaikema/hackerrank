/*
	https://www.hackerrank.com/challenges/priyanka-and-toys
*/

#include <vector>
#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

bool num_in_range(vector<int> vec, int n)
{
	int count = 0;
	for (vector<int>::iterator it = vec.begin(); it != vec.end(); it++)
	{
		if (*it >= n && *it <= n+4)
		{
			count++;
		}
	}
}

// https://www.hackerrank.com/challenges/priyanka-and-toys/forum/comments/122872
int main()
{
	int n;
	cin >> n;

	vector<int> w(n);
	for (int i = 0; i < n; i++)
	{
		cin >> w[i];
	}
	sort(w.begin(), w.end());

	/*
		upper_bound returns an iterator to the first element larger than the third argument
		(in this case w[i]+4).
		This solution just iterates through the sorted vector, 
		and greedily skips ahead to the next position outside of the range, 
		incrementing the count each time.
	*/
	int count = 0;
	for (int i = 0; i < w.size(); i = upper_bound(w.begin(), w.end(), w[i]+4) - w.begin())
	{
		count++;
	}
	cout << count << endl;
}