/*
	https://www.hackerrank.com/challenges/two-arrays
*/

#include <vector>
#include <iostream>
#include <string>
#include <algorithm>
#include <iterator>
#include <sstream>

using namespace std;

/*
	Expects the first vector to be sorted in ascending order,
	and the second vector to be sorted in descending order.
*/
bool can_work(vector<int>& vec1, vector<int>& vec2, int k)
{
	bool flag = true;
	for (int i = 0; i < vec1.size(); i++)
	{
		if (vec1[i] + vec2[i] < k)
		{
			if (i<vec1.size()-1 && vec1[i+1]+vec2[i] >= k && vec1[i]+vec2[i+1] >= k)
			{
				// do nothing
			}
			else
				return false;
		}
	}

	return true;
}

int main()
{
	int q;
	cin >> q;
	stringstream ss;
	for (int i = 0; i < q; i++)
	{
		int n;
		int k;
		cin >> n >> k;
		vector<int> arr1(n);
		vector<int> arr2(n);
		for (int j = 0; j < n; j++)
			cin >> arr1[j];
		for (int j = 0; j < n; j++)
			cin >> arr2[j];
		sort(arr1.begin(), arr1.end());
		sort(arr2.rbegin(), arr2.rend());
		bool flag = can_work(arr1, arr2, k);
		if (flag)
			ss << "YES" << endl;
		else
			ss << "NO" << endl;

	}

	cout << ss.str();
}