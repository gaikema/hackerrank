/*
	https://www.hackerrank.com/challenges/greedy-florist
*/

#include <vector>
#include <iostream>
#include <string>
#include <algorithm>
#include <iterator>
#include <map>
#include <numeric>
#include <list>

using namespace std;

int main()
{
	// Number of flowers needed.
	int n;
	// Number of friends.
	int k;
	cin >> n >> k;
	vector<int> costs;
	for (int i = 0; i < n; i++)
	{
		int c;
		cin >> c;
		costs.push_back(c);
	}

	if (n <= k)
	{
		cout << accumulate(costs.begin(), costs.end(), 0) << endl;
	}
	else
	{
		/*
			The k most expensive flowers will first be purchased, each by one person.
		*/
		sort(costs.begin(), costs.end());
		// Number of flowers purchased by each friend.
		vector<int> num_purchased(k, 1);
		int cost = accumulate(costs.end()-k, costs.end(), 0);
		costs.erase(costs.end()-k, costs.end());
		int curr_friend = 0;
		while (!costs.empty())
		{
			cost += (1+num_purchased[curr_friend]) * *(costs.end()-1);
			num_purchased[curr_friend]++;
			curr_friend = (curr_friend + 1)%k;
			costs.pop_back();
		}

		cout << cost << endl;
	}
}