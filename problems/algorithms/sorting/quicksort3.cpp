/*
	https://www.hackerrank.com/challenges/quicksort3
*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

/**
* Quicksort partition.
* https://en.wikipedia.org/wiki/Quicksort#Algorithm
* https://www.youtube.com/watch?v=aQiWF4E8flQ
*
* @param vector<int>& vec The vector to partition.
* @param lo The wall index.
* @param int hi The pivot index.
*/
int partition(vector<int>& vec, int lo, int hi)
{
	int pivot = vec[hi];

	int j = lo;
	for (int i = lo; i < hi; i++)
	{
		if (vec[i] <= pivot)
		{
			swap(vec[i], vec[j]);
			j++;
		}
	}

	swap(vec[j], vec[hi]);

	for (auto k : vec)
	{
		cout << k << " ";
	}
	cout << endl;

	return j;
}

void quicksort(vector<int>& vec, int lo, int hi)
{
	if (vec.size() <= 1)
		return;
	
	if (lo < hi)
	{
		int p = partition(vec, lo, hi);
		quicksort(vec, lo, p-1);
		quicksort(vec, p+1, hi);
	}
}

int main()
{
	int n;
	cin >> n;

	vector<int> ar(n);
	for (int i = 0; i < n; i++)
	{
		cin >> ar[i];
	}

	quicksort(ar, 0, ar.size() - 1);
	
	return 0;
}