/*
	https://www.hackerrank.com/challenges/runningtime
*/

#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

using namespace std;

/*
	Insertion sort starts with an unsorted input list and an empty output list.
	Each iteration, the algorithm removes an element from the input list
	and finds its place in the output list (which is sorted).
	It repeats until nothing remains in the input list.

	https://gist.github.com/TexAgg/90ee272c64bb496821f5
*/
void insertionSort(vector<int> ar) 
{
	int count = 0;

	for (int i = 1; i < ar.size(); i++)
	{
		int temp = ar[i];
		int j = i;
		for (;j > 0 && temp < ar[j-1]; j--)
		{
			ar[j] = ar[j-1];
			count++;
		}
		ar[j] = temp;
	}

	cout << count;
}

int main(void) 
{
	vector <int>  _ar;
	int _ar_size;
	cin >> _ar_size;
	for(int _ar_i=0; _ar_i<_ar_size; _ar_i++) 
	{
		int _ar_tmp;
		cin >> _ar_tmp;
		_ar.push_back(_ar_tmp); 
	}

	insertionSort(_ar);
	return 0;
}