/*
	https://www.hackerrank.com/challenges/quicksort2
*/

#include <vector>
#include <iostream>
#include <sstream>

using namespace std;

void partition(vector<int>& vec)
{
	int n = vec.size();
	if (n == 0 || n == 1)
		return;

	vector<int> left, right, pivot;
	int p = vec[0];

	for (int i = 0; i < n; i++)
	{
		if (vec[i] < p)
		{
			left.push_back(vec[i]);
		}
		else if (vec[i] > p)
		{
			right.push_back(vec[i]);
		}
		else if (vec[i] == p)
		{
			pivot.push_back(vec[i]);
		}
	}

	// Recursively call partition.
	partition(left);
	partition(right);
	partition(pivot);

	// Combine the subvectors.
	vec.clear();
	vec.insert(vec.end(), left.begin(), left.end());
	vec.insert(vec.end(), pivot.begin(), pivot.end());
	vec.insert(vec.end(), right.begin(), right.end());

	// Print the vector.
	for (auto k : vec)
	{
		cout << k << " ";
	}
	cout << endl;
}

void quickSort(vector<int> &arr) 
{
	// Complete this function.
	partition(arr);
}

int main()
{
	int n;
	cin >> n;

	vector<int> arr(n);
	for(int i = 0; i < (int)n; ++i) 
	{
		cin >> arr[i];
	}

	quickSort(arr);

	return 0;
}