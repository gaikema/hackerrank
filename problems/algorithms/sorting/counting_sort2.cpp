/*
	https://www.hackerrank.com/challenges/countingsort2
*/

#include <iostream>
#include <vector>
#include <map>
#include <sstream>

using namespace std;

int main()
{
	int n;
	cin >> n;

	vector<int> ar(n);
	map<int, int> counts;

	for (int i = 0; i < n; i++)
	{
		cin >> ar[i];
		counts[ar[i]]++;
	}

	stringstream ss;

	for (map<int, int>::iterator it = counts.begin(); it != counts.end(); it++)
	{
		for (int i = 0; i < it->second; i++)
		{
			ss << it->first << " ";
		}
	}

	cout << ss.str() << endl;
	
	return 0;
}