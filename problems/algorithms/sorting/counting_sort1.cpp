/*
	https://www.hackerrank.com/challenges/countingsort1
*/

#include <vector>
#include <iostream>
#include <map>
#include <sstream>

using namespace std;

int main()
{
	int n;
	cin >> n;

	vector<int> ar(n);
	map<int, int> counts;

	for (int i = 0; i < n; i++)
	{
		cin >> ar[i];
		counts[ar[i]]++;
	}

	stringstream ss;

	for (int i = 0; i <= 99; i++)
	{
		ss << counts[i] << " ";	
	}

	cout << ss.str();
	
	return 0;
}