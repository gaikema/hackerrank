/*
	https://www.hackerrank.com/challenges/tutorial-intro
*/

#include <vector>
#include <iostream>
#include <iterator>
#include <algorithm>

using namespace std;

int main()
{
	int v,n;
	vector<int> ar;

	cin >> v;
	cin >> n;

	for (int i = 0; i < n; i++)
	{
		int k;
		cin >> k;
		ar.push_back(k);
	}

	vector<int>::iterator it = find(ar.begin(), ar.end(), v);
	cout << distance(ar.begin(), it);

	return 0;
}