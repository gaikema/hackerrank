/*
	https://www.hackerrank.com/challenges/countingsort3
*/

#include <iostream>
#include <vector>
#include <map>
#include <sstream>

using namespace std;

int main()
{
	int n;
	cin >> n;
	cin.ignore();

	vector<int> ar(n);
	map<int, int> counts;

	for (int i = 0; i < n; i++)
	{
		int b;
		string temp, temp2;
		getline(cin, temp);
		stringstream temp_ss(temp);
		temp_ss >> b >> temp2;
		counts[b]++;
	}

	stringstream ss;
	
	int total_count = 0;
	for (int i = 0; i <= 99; i++)
	{
		total_count += counts[i];
		ss << total_count << " ";
	}

	cout << ss.str() << endl;

	return 0;
}