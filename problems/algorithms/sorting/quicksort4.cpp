/*
	https://www.hackerrank.com/challenges/quicksort4
*/

#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

void insertion_sort(vector<int>& vec, int& count)
{
	int n = vec.size();
	for (int i = 0; i < n; i++)
	{
		int j = i;
		while (j > 0 && vec[j-1] > vec[j])
		{
			swap(vec[j], vec[j-1]);
			count++;
			j--;
		}
	}
}

int partition(vector<int>& vec, int lo, int hi, int& count)
{
	int pivot = vec[hi];

	int j = lo;
	for (int i = lo; i < hi; i++)
	{
		if (vec[i] <= pivot)
		{
			swap(vec[i], vec[j]);
			j++;
			count++;
		}
	}

	swap(vec[j], vec[hi]);
	count++;

	return j;
}

void quicksort(vector<int>& vec, int lo, int hi, int& count)
{
	if (vec.size() <= 1)
		return;
	
	if (lo < hi)
	{
		int p = partition(vec, lo, hi, count);
		quicksort(vec, lo, p-1, count);
		quicksort(vec, p+1, hi, count);
	}
}

int main()
{
	int n;
	cin >> n;

	vector<int> ar(n);

	for (int i = 0; i < n; i++)
	{
		cin >> ar[i];
	}

	vector<int> ar2 = ar;
	int quick_count = 0;
	int insertion_count = 0;

	quicksort(ar, 0, ar.size() - 1, quick_count);
	insertion_sort(ar2, insertion_count);

	cout << insertion_count - quick_count << endl;
	
	return 0;
}