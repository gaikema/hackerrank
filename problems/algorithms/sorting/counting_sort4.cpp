/*
	https://www.hackerrank.com/challenges/countingsort4
*/

#include <iostream>
#include <vector>
#include <map>
#include <sstream>

using namespace std;

// Small helper class.
struct Element
{
	int value;
	bool print = true;
	string str;
};

bool operator==(const Element& lhs, const Element& rhs)
{
	return (lhs.value == rhs.value && lhs.str == rhs.str);
}

// Needed to use map.
bool operator<(const Element& lhs, const Element& rhs)
{
	return (lhs.value < rhs.value);
}

// Method to print the Elements.
ostream& operator<<(ostream& os, Element& e)
{
	os << e.value << ": " << e.str;
	return os;
}

// https://en.wikipedia.org/wiki/Counting_sort
int main()
{
	int n;
	cin >> n;
	cin.ignore();

	vector<Element> ar(n);
	map<Element, int> counts;
	for (int i = 0; i < n; i++)
	{
		string line;
		getline(cin, line);
		stringstream parser(line);

		parser >> ar[i].value >> ar[i].str;
		// Only print the second half.
		if (i < n/2)
			ar[i].print = false;

		// Get the counts of each element.
		counts[ar[i]]++;
	}

	// All the numbers are supposed to be less than 100.
	int k = 100;
	int total = 0;
	for (int i = 0; i < k; i++)
	{
		
	}

	vector<Element> c;

	for (int i = 0; i < c.size(); i++)
	{
		cout << c[i] << endl;
	}
	
	return 0;
}