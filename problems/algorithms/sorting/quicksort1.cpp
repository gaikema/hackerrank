/*
	https://www.hackerrank.com/challenges/quicksort1
*/

#include <sstream>
#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

/**
* @param vector<int> ar The array to quicksort.
* @param bool print Whether or not to print the sorted array.
*/
void partition(vector<int> ar, bool print) 
{
	int p = ar[0];
	vector<int> left, right, pivot;

	for (int i = 0; i < ar.size(); i++)
	{
		if (ar[i] == p)
			pivot.push_back(ar[i]);
		else if (ar[i] < p)
			left.push_back(ar[i]);
		else if (ar[i] > p)
			right.push_back(ar[i]);
	}

	if (print)
	{
		stringstream ss;
		for (int i = 0; i < left.size(); i++)
			ss << left[i] << " ";
		for (int i = 0; i < pivot.size(); i++)
			ss << pivot[i] << " ";
		for (int i = 0; i < right.size(); i++)
			ss << right[i] << " ";
		cout << ss.str();
	}
}

int main(void) 
{
	vector<int>  _ar;
	int _ar_size;
	cin >> _ar_size;

	for(int _ar_i=0; _ar_i<_ar_size; _ar_i++) 
	{
		int _ar_tmp;
		cin >> _ar_tmp;
		_ar.push_back(_ar_tmp); 
	}

	partition(_ar, true);

	return 0;
}