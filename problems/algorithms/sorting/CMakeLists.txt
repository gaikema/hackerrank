CMAKE_MINIMUM_REQUIRED(VERSION 2.6.0)
project(sorting)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -std=c++11")

option(INCLUDE_INTRO "Whether or not to include intro in the build." ON)
if(INCLUDE_INTRO)
	add_executable(intro intro.cpp)
endif()

option(INCLUDE_INSERTION1 "Include insertion1." ON)
if(INCLUDE_INSERTION1)
	add_executable(insertion1 insertion1.cpp)
endif()

option(INCLUDE_RUNTIME "Include runtime." ON)
if(INCLUDE_RUNTIME)
	add_executable(runtime runtime.cpp)
endif()

option(INCLUDE_QUICKSORT1 "Include quicksort1." ON)
if(INCLUDE_QUICKSORT1)
	add_executable(quicksort1 quicksort1.cpp)
endif()

option(INCLUDE_QUICKSORT2 "Include quicksort2." ON)
if(INCLUDE_QUICKSORT2)
	add_executable(quicksort2 quicksort2.cpp)
endif()

option(INCLUDE_QUICKSORT3 "Include quicksort3." ON)
if(INCLUDE_QUICKSORT3)
	add_executable(quicksort3 quicksort3.cpp)
endif()

option(INCLUDE_QUICKSORT4 "Include quicksort4." ON)
if(INCLUDE_QUICKSORT4)
	add_executable(quicksort4 quicksort4.cpp)
endif()

option(INCLUDE_COUNTINGSORT1 "Include counting_sort1." ON)
if(INCLUDE_COUNTINGSORT1)
	add_executable(counting_sort1 counting_sort1.cpp)
endif()

option(INCLUDE_COUNTINGSORT2 "Include counting_sort2." ON)
if(INCLUDE_COUNTINGSORT2)
	add_executable(counting_sort2 counting_sort2.cpp)
endif()

option(INCLUDE_COUNTINGSORT3 "Include counting_sort3." ON)
if(INCLUDE_COUNTINGSORT3)
	add_executable(counting_sort3 counting_sort3.cpp)
endif()

option(INCLUDE_COUNTINGSORT4 "Include counting_sort4." ON)
if(INCLUDE_COUNTINGSORT4)
	add_executable(counting_sort4 counting_sort4.cpp)
endif() 