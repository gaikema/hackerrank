/*
	https://www.hackerrank.com/challenges/equal-stacks
*/

#include <vector>
#include <iostream>
#include <algorithm>
#include <queue>
#include <iterator>

using namespace std;

bool areHeightsSame(vector<int> vec)
{
	/*
	cout << "vec1=" << vec[0] << endl;
	cout << "vec2=" << vec[1] << endl;
	cout << "vec3=" << vec[2] << endl;
	*/
	if (vec[0] == vec[1] && vec[0] == vec[2])
		return true;
	else
		return false;
}

// https://www.hackerrank.com/challenges/equal-stacks/forum/comments/151689
int main()
{
	int n1;
	int n2;
	int n3;
	cin >> n1 >> n2 >> n3;

	vector<int> heights(3);
	vector<queue<int>> stacks;
	// Fill array with empty stacks.
	for(int i = 0; i < 3; i++)
	{
		queue<int> stint;
		stacks.push_back(stint);
	}

	vector<int> h1(n1);
	for(int h1_i = 0;h1_i < n1;h1_i++)
	{
		cin >> h1[h1_i];
		heights[0] += h1[h1_i];
		stacks[0].push(h1[h1_i]);
	}

	vector<int> h2(n2);
	for(int h2_i = 0;h2_i < n2;h2_i++)
	{
		cin >> h2[h2_i];
		heights[1] += h2[h2_i];
		stacks[1].push(h2[h2_i]);
	}

	vector<int> h3(n3);
	for(int h3_i = 0;h3_i < n3;h3_i++)
	{
		cin >> h3[h3_i];
		heights[2] += h3[h3_i];
		stacks[2].push(h3[h3_i]);
	}

	/*
		While the heights are different, 
		remove the top from the tallest stack
		and record the change in the heights vector.
	*/

	while(!areHeightsSame(heights))
	{
		// Get the stack with the tallest height.
		int index = distance(heights.begin(), max_element(heights.begin(), heights.end()));
		//cout << "index=" << index << endl;
		int top = stacks[index].front();
		//cout << "top=" << top << endl;
		//cout << "\n";
		stacks[index].pop();

		heights[index] -= top;
	}

	cout << heights[0];

	return 0;
}