/*
	https://www.hackerrank.com/challenges/save-the-prisoner
*/

#include <iostream>
#include <sstream>

using namespace std;

int main()
{
	int t;
	cin >> t;
	cin.ignore();

	stringstream out;
	for (int i = 0; i < t; i++)
	{
		string temp;
		getline(cin, temp);
		stringstream line(temp);
		// The number of prisoners.
		int n;
		// The number of sweets.
		int m;
		// prisoner ID.
		int s;
		line >> n >> m >> s;

		int num = m % n + s - 1;
		out << num << endl;
	}

	cout << out.str();
	
	return 0;
}