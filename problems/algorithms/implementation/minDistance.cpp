/*
	https://www.hackerrank.com/challenges/minimum-distances
*/

#include <vector>
#include <iostream>
#include <iterator>
#include <algorithm>
#include <unordered_map>
#include <limits>
#include <cmath>

using namespace std;

// https://www.hackerrank.com/challenges/minimum-distances/forum/comments/152201
int main()
{
	int n;
	cin >> n;
	vector<int> a(n);
	// Hash table of each element and its distance.
	unordered_map<int, int> hash;
	// Running minimum distance.
	int min_distance = numeric_limits<int>::max();

	/*
		Put each element and its distance in the hash table,
		unless the element has been seen before,
		in which case, find the distance from its current
		occurence to its previous occurence.
		If that distance is less than min_distance,
		update min_distance.
	*/
	for(int i = 0; i < n; i++)
	{
		cin >> a[i];

		// Check if hash contains a[i].
		if (hash.find(a[i]) != hash.end())
		{
			int x = hash.at(a[i]);
			int dist = abs(x - i);
			if (dist < min_distance)
				min_distance = dist;
		}
		else
		{
			// If it hasn't previously been seen, add it to the hash table.
			hash.insert(pair<int, int>(a[i], i));
		}
	}

	// The problem specifies that -1 be returned.
	if (min_distance == numeric_limits<int>::max())
		min_distance = -1;

	cout << min_distance << endl;

	return 0;
}