/*
	https://www.hackerrank.com/challenges/designer-pdf-viewer
*/

#include <set>
#include <map>
#include <string>
#include <iostream>

using namespace std;

int main()
{
	map<char, int> alphabet;
	set<int> heights;
	for (int i = 0; i < 26; i++)
	{
		// http://www.asciitable.com/
		char current_char = (char) i+97;
		cin >> alphabet[current_char];
	}
	string str;
	cin >> str;
	for (string::iterator it = str.begin(); it != str.end(); it++)
	{
		heights.insert(alphabet[*it]);
	}
	cout << *heights.rbegin() * str.length() << endl;
}