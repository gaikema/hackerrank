/*
	https://www.hackerrank.com/challenges/maximizing-xor
*/

#include <string>
#include <iostream>
#include <algorithm>
#include <limits>

using namespace std;

int main()
{
	int l, r;
	cin >> l >> r;

	int current_max = numeric_limits<int>::min();
	for (int i = l; i <= r; i++)
	{
		for (int j = i; j <= r; j++)
		{
			current_max = max(current_max, i^j);
		}
	}

	cout << current_max;
}