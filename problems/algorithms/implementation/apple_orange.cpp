/*
	https://www.hackerrank.com/challenges/apple-and-orange
*/

#include <string>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	// Left side of the house.
	int s;
	// Right side of the house.
	int t;
	cin >> s >> t;
	// Apple tree.
	int a;
	// Orange tree.
	int b;
	cin >> a >> b;
	// Number of apples.
	int m;
	// Number of oranges.
	int n;
	cin >> m >> n;

	vector<int> apples(m);
	vector<int> oranges(n);
	int apples_on_roof = 0;
	int oranges_on_roof = 0;
	for (int i = 0; i < m; i++)
	{
		cin >> apples[i];
		int dist = a + apples[i];
		if (dist >= s && dist <= t)
			apples_on_roof++;
	}
	for (int i = 0; i < n; i++)
	{
		cin >> oranges[i];
		int dist = b + oranges[i];
		if (dist >= s && dist <= t)
			oranges_on_roof++;
	}

	cout << apples_on_roof << endl << oranges_on_roof << endl;
}