#!/bin/python

import sys

a,b,c,d,e = raw_input().strip().split(' ')
a,b,c,d,e = [int(a),int(b),int(c),int(d),int(e)]
l = sorted([a,b,c,d,e])
low = 0
high = 0
for i in range(0,5):
    if i != 0:
        high += l[i]
    if i != 4:
        low += l[i]
print str(low) + " " + str(high)
