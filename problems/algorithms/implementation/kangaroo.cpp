#include <iostream>
#include <math.h>

using namespace std;

int main()
{
	int x1;
	int v1;
	int x2;
	int v2;

	cin >> x1 >> v1 >> x2 >> v2;

	/*
		Solve Diophantine equation x1+v1*t = x2+v2*t

		We have x1-x2 = t*(v2-v1)
	*/

	double intpart;
	double t = (x1 - x2) / (v2 * 1.0 - v1);
	
	// http://stackoverflow.com/a/1521682/5415895
	if (modf(t, &intpart) == 0.0 && t>0)
		cout << "YES";
	else
		cout << "NO";

	return 0;
}