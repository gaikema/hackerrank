/*
	https://www.hackerrank.com/challenges/almost-sorted
*/

#include <string>
#include <iostream>
#include <vector>

using namespace std;

// https://www.hackerrank.com/challenges/almost-sorted/forum/comments/50674
int main()
{
	int n;
	cin >> n;
	vector<int> vec(n);
	for (int i = 0; i < n; i++)
		cin >> vec[i];

	// curr > prev && cur > next
	int ups = 0;
	// curr < prev && cur < next
	int downs = 0;
	// true indicates an up and false indicates a down.
	map<int, bool> locations;
	for (int i = 1; i < n-1; i++)
	{
		if (vec[i] > vec[i-1] && vec[i] > vec[i+1])
		{
			ups++;
			locations[i] = true;
		}

		if (vec[i] < vec[i-1] && vec[i] < vec[i+1])
		{
			downs++;
			locations[i] = false;
		}
	}
	if (*vec.begin() < *(vec.begin()+1))
	{
		ups++;
		locations[i] = true;
	}
	//if ()
}