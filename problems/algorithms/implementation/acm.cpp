/*
	https://www.hackerrank.com/challenges/acm-icpc-team
*/

#include <string>
#include <vector>
#include <algorithm>
#include <iostream>

using namespace std;

int count_ones(string str)
{
	int count = 0;
	for (string::iterator it = str.begin(); it != str.end(); it++)
	{
		if (*it == '1')
			count++;
	}
	return count;
}

// https://www.hackerrank.com/challenges/acm-icpc-team/forum/comments/56444
int main()
{
	// Number of people.
	int n;
	// Number of topics.
	int m;
	cin >> n >> m;

	for (int i = 0; i < n; i++)
	{
		string str;
		cin >> str;
	}
}