/*
	https://www.hackerrank.com/challenges/cut-the-sticks
*/

#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

bool isNotPositive(int n)
{
	return !(n>0);
}

vector<int> cut(vector<int> vec)
{
	int cutLength = *min_element(vec.begin(), vec.end());
	
	for (int i = 0; i < vec.size(); i++)
		vec[i] -= cutLength;
	vec.erase(remove_if(vec.begin(), vec.end(), isNotPositive), vec.end());
	return vec;
}

int main()
{
	int n;
	cin >> n;
	vector<int> arr(n);
	for(int arr_i = 0;arr_i < n;arr_i++)
		cin >> arr[arr_i];

	while (arr.size() > 0)
	{
		cout << arr.size() << endl;
		arr = cut(arr);
	}

	return 0;
}