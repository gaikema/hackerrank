/*
    https://www.hackerrank.com/challenges/jumping-on-the-clouds
*/

#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

using namespace std;

int main()
{
    int n;
    cin >> n;

    vector<int> c(n);

    for(int c_i = 0; c_i < n; c_i++)
    {
        cin >> c[c_i];
    }

    /*
        The key is to split the path up into safe subsections.
        For each subsection, find the minimum jumps it takes to
        reach the last safe cloud.
        Add up the minimum jumps for each subsection,
        as well as the number of unsafe clouds.

        To do this, we need the length of each safe stretch,
        and the number of dangerous clouds.
    */

    // The lengths of the safe sections.
    vector<int> lengths;

    // Find all stretchs of 0s.
    vector<int>::iterator it = c.begin();
    while (it != c.end())
    {
        vector<int>::iterator prev = it;
        it = find(it+1, c.end(), 1);

        lengths.push_back(distance(prev, it));
    }

    int num_bad = count(c.begin(), c.end(), 1);

    int final_count = 0;
    final_count += num_bad;

    for (int i = 0; i < lengths.size(); i++)
    {
        // Adjust every element but the first; see here: http://cpp.sh/6ytep
        if (i != 0)
            lengths[i]--;
        final_count += (lengths[i] / 2);
    }

    cout << final_count << endl;

    return 0;
}