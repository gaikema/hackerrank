#include <iostream>
#include <string>
#include <map>

using namespace std;

int main()
{
	int n;
	cin >> n;

	map<int, int> counts;
	for (int i = 0; i < n; i++)
	{
		int temp;
		cin >> temp;
		counts[temp]++;
	}

	int count = 0;
	for (map<int, int>::iterator it = counts.begin(); it != counts.end(); it++)
	{
		count += (*it).second/2;
	}
	
	cout << count << endl;
}