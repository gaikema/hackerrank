/*
	https://www.hackerrank.com/challenges/bigger-is-greater
*/

#include <vector>
#include <string>
#include <algorithm>
#include <iostream>
#include <sstream>

using namespace std;

// http://stackoverflow.com/questions/11483060/stdnext-permutation-implementation-explanation
int main()
{
	int t;
	cin >> t;

	stringstream ss;

	for (int i = 0; i < t; i++)
	{
		string perm;
		cin >> perm;

		bool flag = next_permutation(perm.begin(), perm.end());

		if (flag)
		{
			ss << perm << endl;
		}
		else
		{
			ss << "no answer" << endl;
		}

	}

	cout << ss.str();
	
	return 0;
}