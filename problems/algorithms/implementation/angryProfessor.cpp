/*
	https://www.hackerrank.com/challenges/angry-professor
*/

#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

int main(){
	int t;
	cin >> t;
	for(int a0 = 0; a0 < t; a0++)
	{
		// Number of students in each class.
		int n;
		// Cancelation threshold.
		int k;
		cin >> n >> k;
		vector<int> a(n);
		for(int a_i = 0;a_i < n;a_i++)
			cin >> a[a_i];
		
		// Do stuff here.
		// http://www.cplusplus.com/reference/algorithm/copy_if/

		vector<int> copy(a.size());
		auto it = std::copy_if (a.begin(), a.end(), copy.begin(), [](int i){return (i<=0);} );
		copy.resize(std::distance(copy.begin(),it));

		if (copy.size() < k)
			cout << "YES\n";
		else
			cout << "NO\n";

	}
	return 0;
}