/*
    https://www.hackerrank.com/challenges/non-divisible-subset
*/

#include <vector>
#include <iostream>
#include <unordered_map>
#include <algorithm>

using namespace std;

/*
    https://www.hackerrank.com/challenges/non-divisible-subset/forum/comments/150647

    For any number K, 
    the sum of two values, A and B, 
    is divisible by K if the remainders of  A/K + B/K == K mod K.
*/
int main()
{
    int n, k;
    cin >> n >> k;

    if (k == 1)
    {
        cout << 1 << endl;
        return 1;
    }

    vector<int> a(n);
    // A map of integers to their remainder when divided by k.
    unordered_map<int, int> remainders;
    // A map of remainders to the number of times they occur.
    unordered_map<int, int> remainder_counts;

    for (int i = 0; i < n; i++)
    {
        cin >> a[i];
        remainders[a[i]] = a[i] % k;
        remainder_counts[remainders[a[i]]]++;
    }

    int max_size = 0;
    /*
        Loop through the length-2 integer partitions of k.
    */
    int bound;
    if (k % 2 == 0)
        bound = k / 2;
    else
        bound = (k / 2) + 1;
    for (int i = 0; i < bound; i++)
    {
        pair<int, int> p(i, k - i);
        max_size += max(remainder_counts[p.first], remainder_counts[p.second]);
    }

    cout << max_size << endl;
    
    return 0;
}