/*
	https://www.hackerrank.com/challenges/divisible-sum-pairs
*/

#include <iostream>
#include <vector>

using namespace std;

int main()
{
	int n;
	int k;
	cin >> n >> k;
	vector<int> a(n);
	for(int a_i = 0;a_i < n;a_i++)
		cin >> a[a_i];

	int sum = 0;
	for (int j = 0; j < n; j++)
	{
		for (int i = 0; i < j; i++)
		{
			if ( (a[i]+a[j])%k == 0)
				sum += 1;
		}
	}

	cout << sum;

	return 0;
}