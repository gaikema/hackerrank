/*
	https://www.hackerrank.com/challenges/alternating-characters
*/

#include <iostream>
#include <string>
#include <algorithm>
#include <iterator>
#include <cctype>
#include <sstream>

using namespace std;

int main()
{
	int t;
	cin >> t;

	stringstream ss;
	for (int i = 0; i < t; i++)
	{
		string str;
		cin >> str;
		
		int count = 0;
		for (string::iterator ch = str.begin(); ch != str.end()-1; ch++)
		{
			if (*ch == *(ch+1))
				count++;
		}

		ss << count << endl;
	}

	cout << ss.str();
}