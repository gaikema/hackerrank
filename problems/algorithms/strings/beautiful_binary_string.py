#!/bin/python

import sys

n = int(raw_input().strip())
B = raw_input().strip()

# https://www.hackerrank.com/challenges/beautiful-binary-string/forum/comments/153935
# We want to find how many instances of '010' are in the string.
print (n-len(B.replace("010", "")))/3