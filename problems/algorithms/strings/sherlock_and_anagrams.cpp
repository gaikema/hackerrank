/*
	https://www.hackerrank.com/challenges/sherlock-and-anagrams
*/

#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <sstream>

using namespace std;

// http://stackoverflow.com/a/22315816/5415895
int substring_count(string str, string sub)
{
	int count = 0;
	size_t pos = str.find(sub, 0);
	while (pos != string::npos)
	{
		count++;
		pos = str.find(sub, pos+1);
	}

	return count;
}

// http://stackoverflow.com/a/22470047/5415895
vector<string> get_substrings(string str)
{
	int len = str.length();
	vector<string> output;
	for (int i = 0; i < len; i++)
	{
		for (int j = i; j < len-1; j++)
		{
			output.push_back(str.substr(i, j+1));
		}
	}
	return output;
}

bool are_anagrams(string str1, string str2)
{
	sort(str1.begin(), str1.end());
	sort(str2.begin(), str2.end());
	return str1==str2;
}

int main()
{
	int t;
	cin >> t;

	stringstream ss;

	for (int i = 0; i < t; i++)
	{
		string str;
		cin >> str;

		int count = 0;
		vector<string> substrings = get_substrings(str);
		for (int j = 0; j < substrings.size(); j++)
		{
			string temp_str = substrings[j];
			//cout << temp_str << endl;
			for (int k = 0; k < substrings.size(); k++)
			{
				if (are_anagrams(temp_str, substrings[k]))
				{
					count++;
				}
			}
			count--;
		}

		ss << count << endl;
	}
	cout << ss.str();
}