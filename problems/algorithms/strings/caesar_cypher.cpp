/*
	https://www.hackerrank.com/challenges/caesar-cipher-1
*/

#include <iostream>
#include <string>
#include <algorithm>
#include <iterator>
#include <cctype>

using namespace std;

int main()
{
	// Ascii value for 'A'.
	const int UPPER_BASE = 65;
	// Ascii value for 'a'.
	const int LOWER_BASE = 97;
	
	int n;
	string str;
	int k;
	cin >> n;
	cin >> str;
	cin >> k;

	for (string::iterator ch = str.begin(); ch != str.end(); ch++)
	{
		char temp = *ch;
		if (islower(temp))
		{
			int base = static_cast<int>(temp) - LOWER_BASE;
			base = (base+k)%26 + LOWER_BASE;
			temp = static_cast<char>(base);
			*ch = temp;
		}
		else if (isupper(temp))
		{
			int base = static_cast<int>(temp) - UPPER_BASE;
			base = (base+k)%26 + UPPER_BASE;
			temp = static_cast<char>(base);
			*ch = temp;
		}
	}

	cout << str << endl;
}