# https://www.hackerrank.com/challenges/sherlock-and-anagrams/forum/comments/101451
# This works but is too slow.

# http://stackoverflow.com/a/22470047/5415895
def get_substrings(s):
	length = len(s)
	return [s[i:j+1] for i in xrange(length) for j in xrange(i,length)]

# http://www.geeksforgeeks.org/check-whether-two-strings-are-anagram-of-each-other/
# function to check whether two strings are anagram
# of each other
def areAnagram(str1, str2):
	# Get lengths of both strings
	n1 = len(str1)
	n2 = len(str2)
 
	# If lenght of both strings is not same, then
	# they cannot be anagram
	if n1 != n2:
		return 0
 
	# Sort both strings
	str1 = sorted(str1)
	str2 = sorted(str2)
 
	# Compare sorted strings
	for i in xrange(n1):
		if str1[i] != str2[i]:
			return 0
 
	return 1

def main():
	t = int(raw_input())
	for i in range(t):
		s = raw_input()
		count = 0
		subs = get_substrings(s)
		for sub in subs:
			for sub2 in subs:
				if areAnagram(sub, sub2):
					count += 1
			count -= 1
		print count/2

if __name__ == "__main__":
	#print filter_size(get_substrings("abba"))
	main()