/*
	https://www.hackerrank.com/challenges/gem-stones
*/

#include <iostream>
#include <string>
#include <algorithm>
#include <iterator>
#include <cctype>
#include <map>
#include <set>

using namespace std;

bool in_all_maps(char ch, vector<map<char, int>> vec)
{
	bool flag = true;
	for (int i = 0; i < vec.size(); i++)
	{
		flag = flag && vec[i][ch]>0;
	}
	return flag;
}

int main()
{
	int n;
	cin >> n;

	set<char> alphabet;
	vector<map<char,int>> gems(n);

	for (int i = 0; i < n; i++)
	{
		string str;
		cin >> str;

		// Fill the hash tables.
		for (string::iterator ch = str.begin(); ch != str.end(); ch++)
		{
			alphabet.insert(*ch);
			gems[i][*ch]++;
		}
	}

	int count = 0;
	for (set<char>::iterator it = alphabet.begin(); it != alphabet.end(); it++)
	{
		if (in_all_maps(*it, gems))
			count++;
	}

	cout << count << endl;
}