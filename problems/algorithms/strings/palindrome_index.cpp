/*
	https://www.hackerrank.com/challenges/palindrome-index

	This can be optimized:
	https://www.hackerrank.com/challenges/palindrome-index/forum/comments/60201
*/

#include <iostream>
#include <string>
#include <algorithm>
#include <sstream>

using namespace std;

bool is_palindrome(string str)
{
	string backwards = string(str.rbegin(), str.rend());
	return str == backwards;
}

int main()
{
	stringstream ss;
	int t;
	cin >> t;
	vector<int> outs(t, -1);

	for (int i = 0; i < t; i++)
	{
		string str;
		cin >> str;

		if (is_palindrome(str))
			outs[i] = -1;
		else 
		{
			for (int j = 0; j < str.size(); j++)
			{
				string reduced = str;
				reduced.erase(reduced.begin() + j);
				if (is_palindrome(reduced))
				{
					//cout << reduced << endl;
					outs[i] = j;
					break;
				}
			}
		}
	}

	for (int i = 0; i < outs.size(); i++)
	{
		cout << outs[i] << endl;
	}
}