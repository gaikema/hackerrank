/*
	https://www.hackerrank.com/challenges/mars-exploration
*/

#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

int main()
{
	string str;
	cin >> str;

	int counter = 0;
	int bad = 0;

	for (string::iterator ch = str.begin(); ch != str.end(); ch++)
	{
		// The letter should be an S.
		if (counter % 3 == 0 || counter % 3 == 2)
		{
			if (*ch != 'S')
				bad++;
		}
		if (counter % 3 == 1)
		{
			if (*ch != 'O')
				bad++;
		}
		counter++;
	}

	cout << bad << endl;
}