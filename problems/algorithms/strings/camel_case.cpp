/*
	https://www.hackerrank.com/challenges/camelcase
*/

#include <iostream>
#include <string>
#include <algorithm>
#include <sstream>
#include <iterator>
#include <ctype.h>

int main()
{
	std::string str;
	std::cin >> str;

	int count = 1;
	for (std::string::iterator it = str.begin(); it != str.end(); it++)
	{
		if (std::isupper(*it))
			count++;
	}

	std::cout << count << std::endl;
}