/*
	https://www.hackerrank.com/challenges/reduced-string
*/

#include <iostream>
#include <string>
#include <algorithm>
#include <sstream>
#include <iterator>

// https://www.hackerrank.com/challenges/reduced-string/forum/comments/157540
int main()
{
	std::string str;
	std::cin >> str;

	int i = 0;
	while (i < static_cast<int>(str.size()-1))
	{
		if (i > -1 && str[i] == str[i+1])
		{
			str.erase(i, 2);
			i--;
		}
		else
			i++;
	}

	if (str.empty())
		std::cout << "Empty String" << std::endl;
	else
		std::cout << str << std::endl;
}