/*
	https://www.hackerrank.com/challenges/pangrams
*/

#include <iostream>
#include <string>
#include <algorithm>
#include <set>
#include <ctype.h>

int main()
{
	std::string str;
	std::getline(std::cin, str);
	//std::cin.ignore();
	
	std::set<char> seth;
	for (std::string::iterator ch = str.begin(); ch != str.end(); ch++)
	{
		if (*ch != ' ')
		{
			seth.insert(tolower(*ch));
		}
	}

	if (seth.size() == 26)
		std::cout << "pangram" << std::endl;
	else
		std::cout << "not pangram" << std::endl;
}