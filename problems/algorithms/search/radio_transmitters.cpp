/*
	https://www.hackerrank.com/challenges/hackerland-radio-transmitters
*/

#include <string>
#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <iterator>

using namespace std;

int main()
{
	int n, r;
	cin >> n >> r;
	vector<int> houses(n);

	for (int i = 0; i < n; i++)
	{
		cin >> houses[i];
	}
	sort(houses.begin(), houses.end());

	/*
		Use the two pointer algorithm or whatever.

		The way this algorithm works is this:
		we have two iterators, low and high, which we initialize to the first house.
		While high is not pointing to the end, there are 2 things that can happen:
		1. low and high at most r away from eachother, and high+1 is more than r away from low
		2. high is more than r away from low

		* https://tp-iiita.quora.com/The-Two-Pointer-Algorithm
		* https://www.hackerrank.com/challenges/hackerland-radio-transmitters/forum/comments/226343
	*/
	int count = 0;
	vector<int>::iterator low = houses.begin();
	vector<int>::iterator high = houses.begin();
	while (high != houses.end())
	{
		// If high and low are at most r apart and the house after high is more than r away from low.
		if (*high-*low <= r && *(high+1)-*low > r)
		{
			// Place a transmitter on low.
			count++;
			low = high;
			// Move high at least r away from low.
			while (*high-*low <= r)
				high++;
			// Then move low to high.
			low = high;
		}
		// If high is more than r away from low.
		else if (*high-*low > r)
		{
			count++;
			low = high;
		}

		// If the last house is not covered.
		if (*(houses.end()-1)-*high <= r)
		{
			count++;
			break;
		}

		high++;
	}

	cout << count << endl;
}