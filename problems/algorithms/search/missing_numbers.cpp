/*
	https://www.hackerrank.com/challenges/missing-numbers
*/

#include <string>
#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <set>

using namespace std;

// The size complexity could be reduced for sure.
int main()
{
	// Size of the first list.
	int n;
	cin >> n;
	vector<int> l1(n);
	map<int, int> cache1;
	for (int i = 0; i < n; i++)
	{
		cin >> l1[i];
		cache1[l1[i]]++;
	}
	
	// Size of the second list.
	int m;
	cin >> m;
	vector<int> l2(m);
	map<int, int> cache2;
	for (int i = 0; i < m; i++)
	{
		cin >> l2[i];
		cache2[l2[i]]++;
	}

	// Probably don't need a set,
	// but using one to keep only unique values.
	set<int> diffs;
	for (vector<int>::iterator it = l2.begin(); it != l2.end(); it++)
	{
		if (cache1[*it] < cache2[*it])
		{
			diffs.insert(*it);
		}
	}

	for (set<int>::iterator it = diffs.begin(); it != diffs.end(); it++)
	{
		cout << *it << " ";
	}
}