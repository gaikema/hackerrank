/*
	https://www.hackerrank.com/challenges/connected-cell-in-a-grid
*/

#include <string>
#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <set>

using namespace std;

int flood_count(vector< vector<int> >& m, int i, int j, int count)
{
	// Placeholder.
	return 0;
}

int main()
{
	int m, n;
	cin >> n >> m;
	vector< vector<int> > matrix (n, vector<int>(m));
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			cin >> matrix[i][j];
		}
	}
}