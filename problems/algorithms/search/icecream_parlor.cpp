/*
	https://www.hackerrank.com/challenges/icecream-parlor
*/

#include <iostream>
#include <string>
#include <algorithm>
#include <sstream>
#include <iterator>
#include <map>

using namespace std;

/**
* Finds the index of elem in vec.
* Doesn't handle the case where elem isn't in vec.
*/
int get_index(vector<int> vec, int elem)
{
	vector<int>::iterator it = find(vec.begin(), vec.end(), elem);
	return distance(vec.begin(), it);
}

int main()
{
	int t;
	cin >> t;

	stringstream answer;

	for (int i = 0; i < t; i++)
	{
		int m, n;
		// Initialize all of the variables.
		cin >> m >> n;
		vector<int> c(n);
		string temp;
		// Get the prices.
		cin.ignore();
		getline(cin, temp);
		stringstream line(temp);
		for (int j = 0; j < n; j++)
		{
			line >> c[j];
		}

		/*
		Now solve the problem.

		We want to find 2 elements in c that add up to m.
		http://www.geeksforgeeks.org/write-a-c-program-that-given-a-set-a-of-n-numbers-and-another-number-x-determines-whether-or-not-there-exist-two-elements-in-s-whose-sum-is-exactly-x/
		*/

		// Essentially determines if an integer is in c.
		map<int, bool> hash;
		for (int j = 0; j < c.size(); j++)
		{
			int temp = m - c[j];
			if (temp >= 0 && hash[temp])
			{
				// c[j] and temp add up to m.
				int l, r;
				if (temp > c[j])
				{
					l = j;
					vector<int>::iterator it = find(c.begin(), c.end(), temp);
					r = distance(c.begin(), it);
				}
				else if (temp < c[j])
				{
					r = j;
					vector<int>::iterator it = find(c.begin(), c.end(), temp);
					l = distance(c.begin(), it);
				}
				else // temp == c[j]
				{
					l = j;
					vector<int>::iterator it = find(c.begin(), c.end(), temp);
					r = distance(c.begin(), it);
					if (r < l)
						swap(l,r);
				}

				answer << (l+1) << " " << (r+1) << endl;
				break;
			}
			hash[c[j]] = true;
		}
	}

	cout << answer.str();
}