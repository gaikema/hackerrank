/*
	https://www.hackerrank.com/challenges/pairs
*/

#include <string>
#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

using namespace std;

int main()
{
	int n, k;
	cin >> n >> k;

	vector<int> vec(n);
	for (int i = 0; i < n; i++)
	{
		cin >> vec[i];
	}
	sort(vec.begin(), vec.end());

	/*
		https://www.hackerrank.com/challenges/pairs/forum/comments/92192

		Also known as the two-pointer approach.
	*/
	int count = 0;
	// The upper 'pointer'.
	int j = 1;
	// The lower 'pointer'.
	int i = 0;
	while (j < n)
	{
		// Get the difference between the upper and lower pointers.
		int diff = vec[j] - vec[i];

		if (diff == k)
		{
			count++;
			j++;
		}
		else if (diff > k)
			i++;
		else if (diff < k)
			j++;
	}

	cout << count << endl;
}