/*
	https://www.hackerrank.com/challenges/sherlock-and-array
*/

#include <string>
#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <sstream>

using namespace std;

int main()
{
	int t;
	cin >> t;

	stringstream ss;
	for (int i = 0; i < t; i++)
	{
		int n;
		cin >> n;
		vector<int> arr(n);
		for (int j = 0; j < n; j++)
			cin >> arr[j];
		
		if (n == 1)
			ss << "YES" << endl;
		else
		{
			bool flag = false;
			int left = 0;
			int right = accumulate(arr.begin()+1, arr.end(), 0);
			for (vector<int>::iterator it = arr.begin(); it != arr.end(); it++)
			{
				int dist = distance(arr.begin(), it);
				if (left == right)
				{
					flag = true;
					break;
				}
				left += *it;
				right -= *(it+1);
			}
			if (flag)
				ss << "YES" << endl;
			else
				ss << "NO" << endl;
		}
	}

	cout << ss.str();
}