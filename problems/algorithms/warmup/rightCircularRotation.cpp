/*
    https://www.hackerrank.com/challenges/circular-array-rotation
*/

#include <vector>
#include <iostream>
#include <algorithm>
#include <sstream>

using namespace std;

int main()
{
    // The size of the array.
    int n;
    // Number of right rotations.
    int k;
    // Number of queries.
    int q;
    cin >> n >> k >> q;
    // https://www.hackerrank.com/challenges/circular-array-rotation/forum/comments/156729
    k = k % n;

    // The array to rotate.
    vector<int> a (n);
    // The array of queries.
    vector<int> queries (q);

    // Get the values for a.
    for (int i = 0; i < n; i++)
    {
        cin >> a[i];
    }

    /*
        Rotate the array.
        Calling rotate does left-rotation,
        so we need to pass the reverse-iterators to
        do right-rotation.
        http://www.java2s.com/Tutorial/Cpp/0480__STL-Algorithms-Modifying-sequence-operations/Rightrotateasequencebyusingreverseiteratorswiththerotatealgorithm.htm
    */
    rotate(a.rbegin(), a.rbegin() + k, a.rend());

    stringstream ss;

    // Get the queries.
    for (int i = 0; i < q; i++)
    {
        cin >> queries[i];
        ss << a[queries[i]] << endl;
    }

    cout << ss.str();
    
    return 0;
}