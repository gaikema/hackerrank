/*
    https://www.hackerrank.com/challenges/staircase

    This took me like an hour because I'm tired.
    How do people code while tired.
*/

#include <iostream>
#include <sstream>

using namespace std;

int main()
{
    int n;
    cin >> n;

    stringstream ss;
    for (int i = 0; i < n; i++)
    {
        for (int j = i+1; j < n; j++)
        {
            ss << " ";
        }
        for (int j = 0; j <= i; j++)
        {
            ss << "#";
        }
        ss << endl;
    }

    cout << ss.str();

    return 0;
}