/*
    https://www.hackerrank.com/challenges/plus-minus
*/

#include <vector>
#include <iostream>
#include <sstream>

using namespace std;

int main()
{
    int n;
    cin >> n;

    vector<int> arr(n);
    for(int arr_i = 0;arr_i < n;arr_i++)
    {
        cin >> arr[arr_i];
    }

    // Number of negative numbers.
    int sum_n = 0;
    // Number of zeros.
    int sum_z = 0;
    // Number of positive numbers.
    int sum_p = 0;
    for (int i = 0; i < n; i++)
    {
        if (arr[i] < 0)
            sum_n++;
        else if (arr[i] > 0)
            sum_p++;
        else
            sum_z++;
    }

    stringstream ss;
    ss << (float)sum_p/n << endl;
    ss << (float)sum_n/n << endl;
    ss << (float)sum_z/n << endl;

    cout << ss.str();

    return 0;
}