(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      1784,         69]
NotebookOptionsPosition[      1309,         48]
NotebookOutlinePosition[      1748,         65]
CellTagsIndexPosition[      1705,         62]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["1", "Section",
 CellChangeTimes->{3.6917831134134903`*^9}],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"a", "=", 
   RowBox[{"{", 
    RowBox[{"1", ",", "2", ",", "3", ",", "4", ",", "5", ",", "6"}], "}"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"b", "=", 
   RowBox[{"{", 
    RowBox[{"2", ",", "3", ",", "4", ",", "5", ",", "6", ",", "7", ",", "8"}],
     "}"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Union", "[", 
   RowBox[{"a", ",", "b"}], "]"}], "//", "Length"}]}], "Input",
 CellChangeTimes->{{3.6917831212119827`*^9, 3.691783145632824*^9}}],

Cell[BoxData["8"], "Output",
 CellChangeTimes->{{3.6917831416537533`*^9, 3.691783146006586*^9}}]
}, Open  ]]
}, Open  ]]
},
WindowToolbars->"EditBar",
WindowSize->{808, 911},
WindowMargins->{{Automatic, 307}, {-66, Automatic}},
FrontEndVersion->"11.0 for Linux x86 (64-bit) (September 21, 2016)",
StyleDefinitions->FrontEnd`FileName[{"Report"}, "StandardReport.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 64, 1, 67, "Section"],
Cell[CellGroupData[{
Cell[669, 27, 513, 14, 89, "Input"],
Cell[1185, 43, 96, 1, 74, "Output"]
}, Open  ]]
}, Open  ]]
}
]
*)

