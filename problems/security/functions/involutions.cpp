/*
    https://www.hackerrank.com/challenges/security-involution

    Input 1:
    20
    10 9 8 7 6 5 4 3 2 1 11 12 13 14 15 16 17 18 19 20
    Output 1:
    YES
*/

#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
    int n;
    cin >> n;
    // vec is essentially a function.
    // The function is from 1-n, so skip 0.
    vector<int> vec(n+1);

    for (int i = 1; i <= n; i++)
    {
        cin >> vec[i];
    }

    bool flag = true;
    // Just skip i=0.
    for (int i = 1; i < vec.size(); i++)
    {
        /*
            In order to be an involution,
            f(f(x)) == x.
        */
        if (vec[vec[i]] != i)
            flag = false;
    }

    if (flag)
        cout << "YES" << endl;
    else
        cout << "NO" << endl;

    return 0;
}