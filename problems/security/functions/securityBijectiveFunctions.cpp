/*
	https://www.hackerrank.com/challenges/security-bijective-functions
*/

#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

int main() 
{
	int n;
	cin >> n;
	vector<int> vec;

	for (int i = 0; i < n; i++)
	{
		int k;
		cin >> k;
		vec.push_back(k);
	}

	int oldLength = vec.size();

	// http://stackoverflow.com/a/1041939/5415895
	sort( vec.begin(), vec.end() );
	vec.erase( unique( vec.begin(), vec.end() ), vec.end());

	if (vec.size() != oldLength)
		cout << "NO";
	else
		cout << "YES";


	return 0;
}