/*
	https://www.hackerrank.com/challenges/security-inverse-of-a-function
*/

#include <iostream>
#include <algorithm>
#include <unordered_map>
#include <iterator>
#include <sstream>

using namespace std;

int main()
{
	int n;
	cin >> n;

	unordered_map<int, int> fx;

	for (int i = 1; i <= n; i++)
	{
		int j;
		cin >> j;

		fx.insert(pair<int, int>(j, i));
	}

	stringstream ss;
	for (int i = 1; i <= n; i++)
	{
		ss << fx[i] << endl;
	}
	
	cout << ss.str();

	return 0;
}