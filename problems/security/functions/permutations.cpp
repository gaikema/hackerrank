/*
	https://www.hackerrank.com/challenges/security-tutorial-permutations
*/

#include <vector>
#include <iostream>
#include <sstream>

using namespace std;

int main()
{
	int n;
	cin >> n;

	vector<int> f;

	for (int i = 0; i < n; i++)
	{
		int k;
		cin >> k;
		f.push_back(k);
	}

	stringstream ss;
	for (int i = 0; i < n; i++)
	{
		/*
		Since the function is indexed from 1, 
		subtract 1 from the outer value.
		*/
		ss << f[f[i]-1] << endl;
	}

	cout << ss.str();
	
	return 0;
}