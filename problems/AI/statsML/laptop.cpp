/*
    https://www.hackerrank.com/challenges/battery

    All the real work is begin done in laptop.R.
*/

#include <iostream>

using namespace std;

double linear_fit(double x)
{
    double y;
    if (x < 4)
        y = 2 * x;
    else if (x >= 4)
        y = 8;
    return y;
}

int main()
{
    double n;
    cin >> n;

    cout << linear_fit(n) << endl;

    return 0;
}