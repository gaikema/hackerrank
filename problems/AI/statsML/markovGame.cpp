/*
    https://www.hackerrank.com/challenges/markov-snakes-and-ladders
*/

#include <vector>
#include <iostream>
#include <sstream>
#include <string>
#include <utility>

using namespace std;

int main()
{
    int num_configs;

    cin >> num_configs;
    // http://stackoverflow.com/questions/18786575/using-getline-in-c

    for (int i = 0; i < num_configs; i++)
    {
        cin.ignore();
        
        vector<double> die (6);
        int snakes, ladders;
        
        // http://www.cplusplus.com/forum/unices/112048/#msg611733

        /*
            Get the probabilities.
        */
        string line;
        getline(cin, line);
        stringstream ss(line);
        for (int j = 0; j < 6; j++)
        {
            string val;
            getline(ss, val, ',');
            die[j] = stod(val);
        }
        cin.ignore();

        /*
            Get the numbers of ladders and snakes.
        */
        
        for (int j = 0; j < num_configs; j++)
        {
            // http://stackoverflow.com/questions/18786575/using-getline-in-c
        }

        /*
            Now we (should) have all the data,
            so we can do the math stuff.
        */
    }

    return 0;
}