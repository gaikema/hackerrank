from sklearn import linear_model
import numpy
import sys
import pdb
import StringIO

params = map(int, raw_input().split())
f = params[0]
n = params[1]

# http://stackoverflow.com/a/6667361/5415895
x = numpy.zeros((n, f))
y = numpy.zeros(n)

# http://stackoverflow.com/a/522578/5415895
for i in range(n):
#{
	for j, item in enumerate(map(float,raw_input().split())):
	#{
		if j < f:
			x[i][j] = item
		else:
			y[i] = item
	#}	
#}

#pdb.set_trace()

# http://scikit-learn.org/stable/modules/linear_model.html#ordinary-least-squares
clf = linear_model.LinearRegression()
# Get coefficients.
clf.fit(x, y)
coef = clf.coef_

# String Stream
# https://docs.python.org/2/library/stringio.html
output = StringIO.StringIO()
t = int(raw_input())
for i in range(t):
#{
	test = map(float, raw_input().split())
	output.write(numpy.inner(test, coef))
	output.write("\n")
#}

# http://stackoverflow.com/a/10265611/5415895
output.seek(0)
print output.read()
