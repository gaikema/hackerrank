/*
    https://www.hackerrank.com/challenges/stat-warmup
*/

#include <vector>
#include <algorithm>
#include <iostream>
#include <map>
#include <math.h>
#include <sstream>

using namespace std;

int main()
{
    int n;
    cin >> n;

    vector<int> vec(n);
    // The counts associated with each data point.
    map<int, int> counts;
    for (int i = 0; i < n; i++)
    {
        cin >> vec[i];
        counts[vec[i]]++;
    }

    stringstream ss;

    // Find the mean.
    double avg;
    int sum = 0;
    for (auto k : vec)
        sum += k;
    avg = (double)sum/vec.size();
    ss << avg << endl;

    // Find the median.
    sort(vec.begin(), vec.end());
    if (vec.size() % 2 == 1)
        ss << vec[vec.size()/2] << endl;
    else // Average of the two middles.
        ss << 0.5 * (vec[vec.size()/2] + vec[vec.size()/2-1]) << endl;

    // Find the mode.
    // http://stackoverflow.com/a/9371137/5415895
    ss << max_element(counts.begin(), counts.end(), [](const pair<int, int>& p1, const pair<int, int>& p2){return p1.second < p2.second;})->first << endl;

    // Find the standard deviation.
    double stdvar;
    for (auto k : vec)
    {
        stdvar += pow(k-avg,2);
    }
    stdvar = stdvar / vec.size();
    stdvar = sqrt(stdvar);
    ss << stdvar << endl;

    // 95% confidence interval.
    // https://en.wikipedia.org/wiki/Confidence_interval
    const double Z_VALUE = 1.96;
    ss << avg - Z_VALUE*stdvar/sqrt(vec.size()) << " " << avg + Z_VALUE*stdvar/sqrt(vec.size()) << endl;

    cout << ss.str();
    
    return 0;
}