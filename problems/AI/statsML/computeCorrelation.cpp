/*
    https://www.hackerrank.com/challenges/computing-the-correlation
*/

#include <iostream>
#include <vector>
#include <algorithm>
#include <math.h>
#include <numeric>
#include <sstream>
#include <iomanip>

using namespace std;

double mean(const vector<int>& vec)
{
    long sum = 0;
    for (auto k : vec)
    {
        sum += k;
    }
    return (double)sum/vec.size();
}

// Use a given average to save time.
float ssv(const vector<int>& vec, double avg)
{
    long sum_thing = 0;
    for (auto k : vec)
    {
        sum_thing += pow(k-avg,2);
    }
    float d = sum_thing/(vec.size()-1);
    return sqrt(d);
}

int main()
{
    int n;
    cin >> n;

    vector<int> physics(n);
    vector<int> chemistry(n);
    vector<int> math(n);

    for (int i = 0; i < n; i++)
    {
        cin >> math[i] >> physics[i] >> chemistry[i];
    }

    stringstream ss;

    double m_bar = mean(math);
    double p_bar = mean(physics);
    double c_bar = mean(chemistry);

    float s_m = ssv(math, m_bar);
    float s_p = ssv(physics, p_bar);
    float s_c = ssv(chemistry, c_bar);

    // http://stackoverflow.com/a/10908280/5415895
    /*
        I know I should use a function for this,
        but I don't want to recalculate the averages and stuff.
    */

    // r_mp
    double num = inner_product(math.begin(), math.end(), physics.begin(), 0) - n*m_bar*p_bar;
    ss << trunc(100 * num / ((n-1) * s_m * s_p))/100 << endl;

    // r_pc
    num = inner_product(physics.begin(), physics.end(), chemistry.begin(), 0) - n *p_bar*c_bar;
    ss << trunc(100 * num / ((n-1) * s_c * s_p))/100 << endl;

    // r_mc
    num = inner_product(math.begin(), math.end(), chemistry.begin(), 0) - n * m_bar * c_bar;
    ss << trunc(100 * num / ((n-1) * s_m * s_c))/100 << endl;

    cout << ss.str();

    return 0;
}