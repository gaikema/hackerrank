/*
    https://www.hackerrank.com/challenges/botclean

    This way makes more sense to me:
    https://bitbucket.org/gaikema/hackerrank/src/7022e0802a9e3c2072530c4527ad80115d881ddc/problems/AI/bots/clean.cpp?at=master&fileviewer=file-view-default
*/

#include <iostream>
#include <vector>
#include <utility>
#include <sstream>
#include <cmath>
#include <queue>
#include <string>
#include <math.h>
#include <map>

using namespace std;

/**
* Find the nearest dirty spot,
* and move closer to it.
*
* http://www.cplusplus.com/articles/z6vU7k9E/
*/
void next_move(int posr, int posc, vector<string> board) 
{
    if (board[posr][posc] == 'd')
    {
        cout << "CLEAN" << endl;
        return;
    }
    
    map<double, pair<int, int> > dirty;
    // Add all the dirty squares to a map.
    for (int i = 0; i < board.size(); i++)
    {
        for (int j = 0; j < board[i].size(); j++)
        {
            if (board[i][j] == 'd')
            {
                pair<int, int> newPair (i, j);
                double dist = pow(posr - i, 2) + pow(posc - j, 2);
                // The map will be sorted by distance from the robot.
                dirty.insert(pair<double, pair<int, int>> (dist, newPair));
            }
        }
    }

    // Get first element in map.
    int di = posr - dirty.begin()->second.first;
    int dj = posc - dirty.begin()->second.second;

    if (di != 0)
    {
        string move; 
        // Don't forget that the i increases as you move down.
        if (di < 0)
        {
            move = "DOWN";
        }
        else 
        {
            move = "UP";
        }
        cout << move << endl;
    }
    else if (dj != 0)
    {
        string move;
        if (dj > 0)
        {
            move = "LEFT";
        }
        else
        {
            move = "RIGHT";
        }
        cout << move << endl;
    }
}

int main(void)
{
    int pos[2];
    vector<string> board;
    cin >> pos[0] >> pos[1];
    for(int i = 0; i < 5; i++) 
    {
        string s;
        cin >> s;
        board.push_back(s);
    }
    next_move(pos[0], pos[1], board);

    return 0;
}