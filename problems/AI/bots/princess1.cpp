/*
    https://www.hackerrank.com/challenges/saveprincess
*/

#include <iostream>
#include <vector>
#include <utility>
#include <sstream>
#include <cmath>

using namespace std;

int main()
{
    int n;
    cin >> n;

    vector<vector<char>> board(n, vector<char>(n));
    pair<int, int> princess, bot;

    for (int x = 0; x < n; x++)
    {
        for (int y = 0; y < n; y++)
        {
            cin >> board[x][y];
            if (board[x][y] == 'm')
                bot = make_pair(x,y);
            else if (board[x][y] == 'p')
                princess = make_pair(x,y);
        }
    }

    int dx = bot.first - princess.first;
    int dy =  bot.second - princess.second;
    stringstream ss;

    if (dx != 0)
    {
        string move;
        if (dx > 0)
            move = "UP";
        else 
            move = "DOWN";

        for (int i = 0; i < abs(dx); i++)
            ss << move << endl;
    }

    if (dy != 0)
    {
        string move;
        if (dy > 0)
            move = "LEFT";
        else 
            move = "RIGHT";

        for (int i = 0; i < abs(dy); i++)
            ss << move << endl;
    }

    cout << ss.str();
    
    return 0;
}