#!/bin/bash 

# Removes all the _build directories.
# http://stackoverflow.com/a/13032747/5415895
find . -name _build -exec rm -rf {} \;